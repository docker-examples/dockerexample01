**Quellcode**

Zu finden im counter-Ordner. 

Besteht im Prinzip aus einer Main-Klasse welche einen Counter
hochzaehlt (in 1s-Anstaenden) und mit jedem Hochzaehlen den Counter einmal ausgibt.

**Docker**

Zu finden im docker-Ordner.

**Befehle**

Image bauen: `docker build -t dockerdemo/docker01:latest .` (im docker-Ordner)

Container starten: `docker run --rm dockerdemo/docker01` --> Wir sind im Container - wenn wir rausskippen ist Container beendet.

ODER

Container starten (Alternative): `docker run --rm -d dockerdemo/docker01` --> Container läuft im Hintergrund.
Ausgaben von Container beobachten: `docker logs -f <container hash>`